package Slayer.Strategies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Players;

import Slayer.Slayer;

public class Wait implements Strategy
{
    @Override
    public boolean activate()
    {
        /**
         * This strategy activates when our animation isn't -1
         * We assume that we are attacking if it isn't -1
         * Another way to execute this strategy is
         *
         * if (Players.getMyPlayer().getAnimation() != -1)
         * {
         *      return true;
         * {
         * return false;
         *
         * Both are the _exact_ same
         */
        return Players.getMyPlayer().getAnimation() != -1;
    }

    @Override
    public void execute()
    {
    	Slayer.status = "In-Combat";
        // Can you figure out when this sleep condition will finish?
        Time.sleep(new SleepCondition()
        {
            @Override
            public boolean isValid()
            {
                return Players.getMyPlayer().getAnimation() == -1;
            }
            // The max sleep time is lower for this because if the user stops the script, we don't want them waiting a whole 5 seconds
        }, 1000);
    }
}