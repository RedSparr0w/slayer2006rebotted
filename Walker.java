package Slayer;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Equipment;
import org.rev317.min.api.methods.GroundItems;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.wrappers.GroundItem;
import org.rev317.min.api.wrappers.Npc;
import org.rev317.min.api.wrappers.SceneObject;
import org.rev317.min.api.wrappers.Tile;
import org.rev317.min.api.wrappers.TilePath;

import Slayer.Strategies.KillGreenGoblins;
import Slayer.Strategies.KillRedGoblins;
import Slayer.Strategies.KillScorpions;
import Slayer.Strategies.KillSkeletons;
import TaverlyChaos.ChaosDruids;

//It is better practice to separate strategies into another class, but for beginner's sake we'll keep it all in one
//This strategy is in charge of catching fish
public class Walker implements Strategy {

private static final Tile tileBears = new Tile(2975, 3492);
 @Override
 public boolean activate() {
	 	if (Slayer.taskid == 1 && tileBears.distanceTo() > 20) {
	 		return true;
	 	}
	 	if (Slayer.taskid == 2 && KillScorpions.tileScorpions.distanceTo() > 20) {
	 		return true;
	 	}
	 	if (Slayer.taskid == 3 && KillSkeletons.tileSkeletons.distanceTo() > 20) {
	 		return true;
	 	}
	 	if (Slayer.taskid == 4 && KillRedGoblins.tileGoblins.distanceTo() > 20) {
	 		return true;
	 	}
	 	if (Slayer.taskid == 5 && KillGreenGoblins.tileGoblins.distanceTo() > 20) {
	 		return true;
	 	}
	 	if (Slayer.taskid == 6 && KillGreenGoblins.tileGoblins.distanceTo() > 20 && Equipment.isWearing(4167)) {
	 		return true;
	 	}
	 	if (Slayer.taskid == 0 && Slayer.previoustaskid == 1) {
	 		//bear reverse path
	 		return true;
	 	}
	 	if (Slayer.taskid == 0 && Slayer.previoustaskid == 4) {
	 		//goblin reverse path
	 		return true;
	 	}
	 	return false;
 }

 @Override
 public void execute() {
	 if (Slayer.taskid == 0 && Slayer.previoustaskid == 1) {
		 Slayer.status = "Reverse walking to Turael";
		 while(!Variables.walkToBearsFinished1.hasReached()) {
			 Variables.walkToBearsFinished1.traverse();
			 Time.sleep(4000, 4750);
		 }
		Slayer.previoustaskid = 0;
	 }
	 if (Slayer.taskid == 0 && Slayer.previoustaskid == 4) {
		 Slayer.status = "Reverse walking to Turael";
		 while(!Variables.walkToGoblinsFinished.hasReached()) {
			 Variables.walkToGoblinsFinished.traverse();
			 Time.sleep(4000, 4750);
		 }
		 Slayer.previoustaskid = 0;
	 }
if (Slayer.taskid == 1) {
	Slayer.status = "Walking to Bears";
    while(!Variables.walkToBears2.hasReached()) {
    	Variables.walkToBears2.traverse();
    	Time.sleep(4000, 4750);
    }
}
if (Slayer.taskid == 2) {
	if (GetTask.tileTurael.distanceTo() < 20) {
		Keyboard.getInstance().sendKeys("::stuck 1");
		Time.sleep(2500);
	}
	Slayer.status = "Walking to Scorpions";
    while(!Variables.walkToScorpions2.hasReached()) {
    	Variables.walkToScorpions2.traverse();
    	Time.sleep(4000, 4750);
    }
}
if (Slayer.taskid == 3) {
	if (GetTask.tileTurael.distanceTo() < 20) {
		Keyboard.getInstance().sendKeys("::stuck 1");
		Time.sleep(2500);
	}
	Slayer.status = "Walking to Skeletons";
    while(!Variables.walkToSkeletons2.hasReached()) {
    	if (Players.getMyPlayer().getLocation().equals(new Tile(2884, 3398))) {
            SceneObject ladderescape = SceneObjects.getClosest(1759);
            ladderescape.interact(0);
        	//end ladder
            Time.sleep(2000);
    	}
    	Variables.walkToSkeletons2.traverse();
    	Time.sleep(4000, 4750);
    }
}
//Goblin Task
if (Slayer.taskid == 4 || Slayer.taskid == 5) {
	Slayer.status = "Walking to Goblins";
    while(!Variables.walkToRedGoblins2.hasReached()) {
    	Variables.walkToRedGoblins2.traverse();
    	Time.sleep(4000, 4750);
    }
}
//Banshee Task
if (Slayer.taskid == 6 && Equipment.isWearing(4167)) {
	if (GetTask.tileTurael.distanceTo() < 20) {
		Keyboard.getInstance().sendKeys("::stuck 1");
		Time.sleep(2500);
	}
    while(!Variables.walkToBanshees2.hasReached() && Variables.walkToBanshees2.getNextTile() != null) {
    	if (Players.getMyPlayer().getLocation().equals(new Tile(3405, 3506))) {
    		
    		for(SceneObject object : SceneObjects.getAllSceneObjects()){

                if(object != null && object.getId() == 1568){
                	Slayer.status = "Opening Trapdoor";
                    //Do Something With The Object AKA INTERACT WITH THE MOTHER FUCKING TRAPDOOR!
                	object.interact(SceneObjects.Option.FIRST);
                	//Menu.sendAction(502, object.getHash(), 53, 51);
                	Time.sleep(3000);
                    SceneObject ladderenter1 = SceneObjects.getClosest(10698);
                    if (ladderenter1 != null) {
                    	Slayer.status = "Climbing Down Trapdoor";
                    	ladderenter1.interact(SceneObjects.Option.FIRST);	
                    	//Menu.sendAction(502, ladderenter1.getHash(), 53, 51);
                    }
                    Time.sleep(2000);
                }

            }
            Time.sleep(1000);
    	}
    	else if(Players.getMyPlayer().getLocation().equals(new Tile(3405, 9906)) || Players.getMyPlayer().getLocation().equals(new Tile(3405, 9895))) {
            SceneObject ladderenter1 = SceneObjects.getClosest(3444);
            if (ladderenter1 != null) {
            	Slayer.status = "Opening door";
            		Menu.sendAction(502, ladderenter1.getHash(), 53, 39);
            		
            }
            Time.sleep(3500);
    	}
    	else if (Players.getMyPlayer().getLocation().equals(new Tile(3431, 9897)) || Players.getMyPlayer().getLocation().equals(new Tile(3427, 9896))) {
    		Slayer.status = "Opening Door";
    		SceneObject doorenter = SceneObjects.getClosest(3445);
            if (doorenter != null) {
            	Slayer.status = "Opening door";
            		Menu.sendAction(502, doorenter.getHash(), 79, 41);
            	
            }
            Time.sleep(8000);
    	}
    	else if (Players.getMyPlayer().getLocation().equals(new Tile(3440,9889))) {
    		Slayer.status = "Exitting Underground";
    		SceneObject doorenter1 = SceneObjects.getClosest(3443);
            if (doorenter1 != null) {
            		Menu.sendAction(502, doorenter1.getHash(), 56, 30);
            	
            }
            Time.sleep(8000);
    	}
    	else if (Players.getMyPlayer().getLocation().equals(new Tile(3429, 3532)) || Players.getMyPlayer().getLocation().equals(new Tile(3429, 3535))) {
    		
    		for(SceneObject object : SceneObjects.getAllSceneObjects()){

                if(object != null && object.getId() == 4490){
                	Slayer.status = "Opening Slayer Tower Door";
                	object.interact(SceneObjects.Option.FIRST);
                	//Menu.sendAction(502, object.getHash(), 53, 55);
                	Time.sleep(1000);
                }

            }
            Time.sleep(8000);
    	}
    	else
    	{
    	Slayer.status = "Walking to Banshees";
    	Slayer.task = "Banshees";
    	Variables.walkToBanshees2.traverse();
    	Time.sleep(2000, 2750);
    	}
    }
	
}
}
}