package Slayer;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Players;

public class eatFood implements Strategy {
  	 
    @Override
    public boolean activate() {
            return Players.getMyPlayer().getHealth() < Slayer.healthEat;
    }

    @Override
    public void execute() {
    	Slayer.status = "Eating";
    	
        for (int i = 0; i < Inventory.getItems(Slayer.food2).length && Players.getMyPlayer().getHealth() < Slayer.healthEat; i++){
            final int health = Players.getMyPlayer().getHealth();
            Menu.sendAction(74, Slayer.food, Inventory.getItems(Slayer.food2)[0].getSlot(), 3214);
            Time.sleep(new SleepCondition() {
                @Override
                public boolean isValid() {
                    return Players.getMyPlayer().getHealth() > health;
                }
            }, 2000);
        }
    }
}